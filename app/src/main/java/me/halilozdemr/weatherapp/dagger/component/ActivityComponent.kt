package me.halilozdemr.weatherapp.dagger.component

import dagger.Component
import me.halilozdemr.weatherapp.dagger.module.ActivityModule
import me.halilozdemr.weatherapp.dagger.module.ApiModule
import me.halilozdemr.weatherapp.dagger.module.NetModule
import me.halilozdemr.weatherapp.dagger.scope.PerActivity
import me.halilozdemr.weatherapp.ui.city.CityWeatherActivity
import me.halilozdemr.weatherapp.ui.main.MainActivity


@PerActivity
@Component(
    dependencies = [AppComponent::class],
    modules = [NetModule::class, ApiModule::class, ActivityModule::class]
)
interface ActivityComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(cityWeatherActivity: CityWeatherActivity)
}
