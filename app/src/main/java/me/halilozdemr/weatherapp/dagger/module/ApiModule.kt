package me.halilozdemr.weatherapp.dagger.module

import dagger.Module
import dagger.Provides
import me.halilozdemr.weatherapp.dagger.scope.PerActivity
import me.halilozdemr.weatherapp.networking.repo.ApiClient
import retrofit2.Retrofit

@Module
class ApiModule {
    @Provides
    @PerActivity
    fun provideApi(builder: Retrofit.Builder): ApiClient {
        return builder.baseUrl(ApiClient.BASE_URL).build().create(ApiClient::class.java)
    }
}
