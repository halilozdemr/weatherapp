package me.halilozdemr.weatherapp.dagger.module

import android.app.Activity
import dagger.Module
import dagger.Provides
import me.halilozdemr.weatherapp.dagger.scope.PerActivity
import me.halilozdemr.weatherapp.ui.city.CityWeatherPresenter
import me.halilozdemr.weatherapp.ui.main.*


@Module
class ActivityModule(private val mActivity: Activity) {

    @Provides
    internal fun provideActivity(): Activity {
        return mActivity
    }

    @Provides
    @PerActivity
    fun provideMainPresenter(presenter: MainPresenter<MainMvpView>): MainMvpPresenter<MainMvpView> {
        return presenter
    }

    @Provides
    @PerActivity
    fun provideCityWeatherPresenter(presenter: CityWeatherPresenter<CityWeatherMvpView>): CityWeatherMvpPresenter<CityWeatherMvpView> {
        return presenter
    }


}
