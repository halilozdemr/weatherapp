package me.halilozdemr.weatherapp.dagger.component

import android.app.Application
import android.content.Context
import dagger.Component
import me.halilozdemr.weatherapp.dagger.module.AppModule
import javax.inject.Singleton


@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun application(): Application

    fun context(): Context

    object Initializer {

        fun init(application: Application): AppComponent {
            return DaggerAppComponent.builder()
                .appModule(AppModule(application))
                .build()
        }
    }
}
