package me.halilozdemr.weatherapp.data

object IntentTags {
    var TAG_WOEID = "tag.woeid"
    var TAG_TITLE = "tag.title"
}
