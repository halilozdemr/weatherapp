package me.halilozdemr.weatherapp.ui.main


import me.halilozdemr.weatherapp.networking.model.Weather
import me.halilozdemr.weatherapp.ui.base.MvpView

interface CityWeatherMvpView : MvpView {
    fun setUiData(weather: Weather)
}

