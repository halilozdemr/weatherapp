package me.halilozdemr.weatherapp.ui.city

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_city_weather.*
import me.halilozdemr.weatherapp.R
import me.halilozdemr.weatherapp.data.IntentTags
import me.halilozdemr.weatherapp.networking.model.Weather
import me.halilozdemr.weatherapp.ui.base.BaseActivity
import me.halilozdemr.weatherapp.ui.main.CityWeatherMvpPresenter
import me.halilozdemr.weatherapp.ui.main.CityWeatherMvpView
import javax.inject.Inject


class CityWeatherActivity : BaseActivity(), CityWeatherMvpView {

    @set:Inject
    lateinit var presenter: CityWeatherMvpPresenter<CityWeatherMvpView>

    private lateinit var mFirebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_city_weather)
        activityComponent.inject(this)
        presenter.onAttach(this)
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)

        var woeid = intent.getIntExtra(IntentTags.TAG_WOEID, 0)
        var title = intent.getStringExtra(IntentTags.TAG_TITLE)

        presenter.getWeather(woeid)
        recyclerDays.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)


        val bundle = Bundle()
        bundle.putInt(FirebaseAnalytics.Param.ITEM_LOCATION_ID, woeid)
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "city")
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, title)
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
    }


    override fun setUiData(weather: Weather) {
        txtCityName.text = weather.title
        txtParent.text = weather.parent.title

        txtTemp.text = Math.round(weather.consolidatedWeather[0].theTemp).toString()
        txtCircle.visibility = View.VISIBLE

        var imagePath =
            "https://www.metaweather.com" +
                    "/static/img/weather/png/${weather.consolidatedWeather[0].weatherStateAbbr}.png"
        Picasso.get().load(imagePath).into(imgWeather)


        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        recyclerDays.adapter = WeatherListAdapter(weather.consolidatedWeather, this, displayMetrics.widthPixels)

    }


}
