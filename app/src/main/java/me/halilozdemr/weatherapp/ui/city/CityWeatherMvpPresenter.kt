package me.halilozdemr.weatherapp.ui.main

import me.halilozdemr.weatherapp.ui.base.MvpPresenter

interface CityWeatherMvpPresenter<V : CityWeatherMvpView> : MvpPresenter<V> {
    fun getWeather(woeid: Int)

}
