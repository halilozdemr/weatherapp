package me.halilozdemr.weatherapp.ui.city

import android.content.Context
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import me.halilozdemr.weatherapp.networking.model.Weather
import me.halilozdemr.weatherapp.networking.repo.ApiClient
import me.halilozdemr.weatherapp.ui.base.BasePresenter
import me.halilozdemr.weatherapp.ui.main.CityWeatherMvpPresenter
import me.halilozdemr.weatherapp.ui.main.CityWeatherMvpView
import javax.inject.Inject


class CityWeatherPresenter<V : CityWeatherMvpView> @Inject
constructor(apiClient: ApiClient, context: Context) : BasePresenter<V>(apiClient, context), CityWeatherMvpPresenter<V> {
    override fun getWeather(woeid: Int) {

        mvpView.showDialog()

        apiClient.getWeather(woeid).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<Weather> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onSuccess(weather: Weather) {
                    mvpView.hideDialog()
                    mvpView.setUiData(weather)
                }

                override fun onError(e: Throwable) {
                    print(e)
                }

            })
    }

}
