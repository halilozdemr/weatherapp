package me.halilozdemr.weatherapp.ui.base


import androidx.annotation.StringRes

interface MvpView {
    fun showDialog()
    fun hideDialog()
    fun showToast(@StringRes resId: Int)
    fun showToast(message: String)
}
