package me.halilozdemr.weatherapp.ui.city

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_weather.view.*
import me.halilozdemr.weatherapp.R
import me.halilozdemr.weatherapp.networking.model.ConsolidatedWeather
import java.text.SimpleDateFormat


class WeatherListAdapter(private val weatherList: List<ConsolidatedWeather>, val context: Context, private val widthPixels: Int) : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_weather, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.layoutParams.width = widthPixels / 6

        holder.txtTemp.text = Math.round(weatherList[position].theTemp).toString()
        holder.txtDay.text = getDayName(weatherList[position].applicableDate).substring(0, 3)
        var imagePath =
                "https://www.metaweather.com" +
                        "/static/img/weather/png/64/${weatherList[position].weatherStateAbbr}.png"
        Picasso.get().load(imagePath).into(holder.imgWeather)


    }

    override fun getItemCount(): Int {
        return weatherList.size
    }


}

fun getDayName(input: String): String {
    val inFormat = SimpleDateFormat("dd-MM-yyyy")
    val date = inFormat.parse(input)
    val outFormat = SimpleDateFormat("EEEE")
    return outFormat.format(date)
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val txtDay = view.txtDay!!
    val txtTemp = view.txtTemp!!
    val imgWeather = view.imgWeather!!
}



