package me.halilozdemr.weatherapp.ui.main

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_city.view.*
import me.halilozdemr.weatherapp.R
import me.halilozdemr.weatherapp.data.IntentTags
import me.halilozdemr.weatherapp.networking.model.City
import me.halilozdemr.weatherapp.ui.city.CityWeatherActivity

class MainListAdapter(private val cityList: List<City>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_city, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txtCity.text = cityList[position].title

        holder.itemView.setOnClickListener {
            val intent = Intent(context, CityWeatherActivity::class.java)
            intent.putExtra(IntentTags.TAG_WOEID, cityList[position].woeid)
            intent.putExtra(IntentTags.TAG_TITLE, cityList[position].title)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return cityList.size
    }


}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val txtCity = view.txtCity!!
}
