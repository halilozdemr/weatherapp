package me.halilozdemr.weatherapp.ui.main

import me.halilozdemr.weatherapp.ui.base.MvpPresenter

interface MainMvpPresenter<V : MainMvpView> : MvpPresenter<V> {

    fun getCities(lattlong: String)
}
