package me.halilozdemr.weatherapp.ui.base

import android.animation.ObjectAnimator
import android.app.Dialog
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import me.halilozdemr.weatherapp.R
import me.halilozdemr.weatherapp.WeatherApp
import me.halilozdemr.weatherapp.dagger.component.DaggerActivityComponent
import me.halilozdemr.weatherapp.dagger.module.ActivityModule
import me.halilozdemr.weatherapp.dagger.module.ApiModule
import me.halilozdemr.weatherapp.dagger.module.NetModule


abstract class BaseActivity : AppCompatActivity(), MvpView {

    lateinit var activityComponent: DaggerActivityComponent
    private var dialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent = DaggerActivityComponent.builder()
            .activityModule(ActivityModule(this))
            .netModule(NetModule())
            .apiModule(ApiModule())
            .appComponent((application as WeatherApp).appComponent)
            .build() as DaggerActivityComponent

    }


    override fun showDialog() {
        hideDialog()
        dialog = Dialog(this)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_dialog)
        dialog!!.setCancelable(false)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))

        val imageView = dialog!!.findViewById<ImageView>(R.id.img_rotate)

        var anim = ObjectAnimator.ofFloat(imageView, "rotation", 0f, 360f)

        anim.repeatCount = ObjectAnimator.INFINITE;
        anim.repeatMode = ObjectAnimator.RESTART;

        anim.duration = 1000
        anim.start()
        dialog!!.show()
    }

    override fun hideDialog() {
        if (dialog == null) return
        dialog!!.dismiss()
    }

    override fun showToast(message: String) {
        toast(message)
    }


    override fun showToast(@StringRes resId: Int) {
        toast(getString(resId))
    }

    private fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

}
