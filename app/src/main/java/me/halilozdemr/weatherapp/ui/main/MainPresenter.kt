package me.halilozdemr.weatherapp.ui.main

import android.content.Context
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import me.halilozdemr.weatherapp.networking.model.City
import me.halilozdemr.weatherapp.networking.repo.ApiClient
import me.halilozdemr.weatherapp.ui.base.BasePresenter
import javax.inject.Inject


class MainPresenter<V : MainMvpView> @Inject
constructor(apiClient: ApiClient, context: Context) : BasePresenter<V>(apiClient, context), MainMvpPresenter<V> {
    override fun getCities(lattlong: String) {

        mvpView.showDialog()

        apiClient.cities(lattlong).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<List<City>> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onSuccess(cities: List<City>) {

                    mvpView.hideDialog()
                    mvpView.setAdapter(cities)
                }

                override fun onError(e: Throwable) {
                    print(e)
                    mvpView.showToast(e.localizedMessage)
                }

            })
    }


}
