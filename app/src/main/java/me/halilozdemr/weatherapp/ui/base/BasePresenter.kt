package me.halilozdemr.weatherapp.ui.base


import android.content.Context
import me.halilozdemr.weatherapp.networking.repo.ApiClient
import java.io.IOException
import java.io.InputStream

import javax.inject.Inject

open class BasePresenter<V : MvpView> @Inject
constructor(val apiClient: ApiClient, val context: Context) : MvpPresenter<V> {

    lateinit var mvpView: V
        private set


    override fun onAttach(mvpView: V) {
        this.mvpView = mvpView
    }

    /**
     * https://stackoverflow.com/a/45177069/6219336
     */
    fun inputStreamToString(inputStream: InputStream): String? {
        return try {
            val bytes = ByteArray(inputStream.available())
            inputStream.read(bytes, 0, bytes.size)
            String(bytes)
        } catch (e: IOException) {
            null
        }

    }
}
