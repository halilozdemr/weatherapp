package me.halilozdemr.weatherapp.ui.base

interface MvpPresenter<V : MvpView> {
    fun onAttach(mvpView: V)
}
