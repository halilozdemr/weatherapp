package me.halilozdemr.weatherapp.ui.main


import me.halilozdemr.weatherapp.networking.model.City
import me.halilozdemr.weatherapp.ui.base.MvpView

interface MainMvpView : MvpView {
    fun setAdapter(cities: List<City>)
}

