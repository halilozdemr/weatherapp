package me.halilozdemr.weatherapp.networking.repo

import io.reactivex.Single
import me.halilozdemr.weatherapp.networking.model.City
import me.halilozdemr.weatherapp.networking.model.Weather
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiClient {

    @GET("location/search")
    fun cities(@Query("lattlong") lattlong: String): Single<List<City>>


    @GET("location/{woeid}/")
    fun getWeather(@Path("woeid") woeid: Int): Single<Weather>


    companion object {
        const val BASE_URL = "https://www.metaweather.com/api/"
    }

}
