package me.halilozdemr.weatherapp


import android.app.Application
import me.halilozdemr.weatherapp.dagger.component.AppComponent

class WeatherApp : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = AppComponent.Initializer.init(this)
    }
}
